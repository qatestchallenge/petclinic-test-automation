# petclinic-test-automation

### Project aims
The project developed to cover PetClinic application with integration tests using BDD approach

## Base workflow
1. Checkout project from GIT
    ```commandline
    git clone https://gitlab.com/qatestchallenge/petclinic-test-automation.git
    ```
2. [OPTIONAL] Then you need to connect  **petclinic-test-automation**:
    1. Open `petclinic-test-automation` project in IDE
    2. Click on `Maven Projects` bar
    3. Click on `Add Maven Project`
    4. Go to `petclinic-test-automation` folder and select `pom.xml file`
    5. Click OK
    6. Navigate to Project Setting-> Build-> Build Tools-> Maven-> Choose maven version 3.6.0
    7. Navigate to Project Setting-> Build-> Compiler-> Java Compiler-> Choose Java 17
3. Run tests using maven
   ```commandline
   mvn clean test
   ```

### Technical description
- [Java 17](https://openjdk.java.net/projects/jdk/17/)
- [IntelliJ Idea](https://www.jetbrains.com/idea/) highly recommended IDE
- [Maven 3](https://maven.apache.org/guides/) - project management tool
- [RestAssured](https://rest-assured.io/) - REST APIs testing tool
- [JUnit 5](https://junit.org/junit5/docs/current/user-guide/) - test engine
- [Cucumber](https://cucumber.io/docs/guides/) - BDD tool
- [git](https://git-scm.com/) - source code management tool
- [Java-Faker](https://github.com/DiUS/java-faker) - fake test data preparation tool
- [Allure Framework](https://docs.qameta.io/allure/#_cucumber_jvm) - test reporting tool

### How to run application under test:
Running the application under test is the entry point for tests.

Repository for PetClinic application:
`https://github.com/spring-petclinic/spring-petclinic-rest`
There is detailed information how to configure and run application
Below I put a shortcut how to run PetClinic:
   ```commandline
   git clone https://github.com/spring-petclinic/spring-petclinic-rest.git
   cd spring-petclinic-rest
   ./mvnw spring-boot:run
   ```
When application is run api documentation can be found here:
`http://localhost:9966/petclinic/swagger-ui.html`

## Running

### Local and CI
Run tests with command line parameters:
   ```commandline
   clean test 
   ```

### Maven usage
You can `set any properties` via Maven
Description of maven options (cmd line args):
* `clean` - clean up all temporary data from previous run
* `test` - execute tests
* `-Dcucumber.filter.tags=@smoke` - maven parameter (java style). Explanation:
   * `-D` - indicate that it java system property
   * `cucumber.filter.tags` - property name
   * `@smoke` - property value. Please use double quotes if value has spaces `"value with spaces"`

### API config properties
Project properties are located under `src/main/resources` folder. Properties could be changed directly in files or as
command line parameters for Maven. `config.properties` - contains properties for building application under test API base URI

### Allure properties
| Property | Default value          | remark                                        |
| --- |------------------------|-----------------------------------------------|
|allure.results.directory | target/allure-results  | path to generate allure report in project dir |

### Cucumber properties
| Property | Default value                                           | remark                                     |
| --- |---------------------------------------------------------|--------------------------------------------|
|cucumber.publish.enabled | true                                                    | enable publishing of test results          |
|cucumber.publish.quiet | true                                                    | suppress publish banner after test execution |
|cucumber.execution.dry-run | false                                                   | tests dry-run option                       |
|cucumber.snippet-type | camelcase                                               | snippet-type value                         |
|cucumber.plugin | io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm,pretty | comma separated plugin strings             |
|cucumber.filter.tags | @regression                                             | cucumber tag expression                    |

## Reporting

### Allure
* `target/allure-reports` - .json files with reports
* report generated after using `allure serve target/allure-results` command and opened in default browser
* allure report Overview screen for generated report show general results per test execution: ![allure_overview_screen](allure_overview_screen.png)
* allure report Suites screen for generated report show suites structure and execution results per feature file:![allure_suites_screen](allure_suites_screen.png)
* allure report Categories screen show failed test by category (e.g. product defects):![allure_categories_screen](allure_categories_screen.png)
