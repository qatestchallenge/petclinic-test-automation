package org.petclinic.automation.data.factories;

import com.github.javafaker.Faker;
import org.petclinic.automation.data.dto.OwnerDTO;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class OwnersFactory {
    private static final Logger LOGGER = LogManager.getLogger(OwnersFactory.class);
    private static final Faker FAKER = new Faker();

    private OwnersFactory() {
    }

    public static OwnerDTO getBasicOwner() {
        OwnerDTO ownerDTO = OwnerDTO.builder()
                .firstName(FAKER.name().firstName())
                .lastName(FAKER.name().lastName())
                .address(FAKER.address().fullAddress())
                .city(FAKER.address().city())
                .telephone(RandomStringUtils.randomNumeric(8))
                .pets(List.of())
                .build();
        LOGGER.info(String.format("New instance of OwnerTDO is created: %s", ownerDTO.toString()));
        return ownerDTO;
    }

}
