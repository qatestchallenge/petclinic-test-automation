package org.petclinic.automation.data.factories;

import com.github.javafaker.Faker;
import org.petclinic.automation.data.dto.PetDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.petclinic.automation.utils.DateTimeUtils;
import org.petclinic.automation.utils.RandomUtils;
import org.petclinic.automation.services.PetsApiService;

import java.time.ZonedDateTime;
import java.util.List;

public class PetsFactory {
    private static final Logger LOGGER = LogManager.getLogger(PetsFactory.class);
    private static final Faker FAKER = new Faker();

    private PetsFactory() {
    }

    public static PetDTO getBasicPet() {
        PetDTO petDTO = PetDTO.builder()
                .name(FAKER.name().firstName())
                .birthDate(DateTimeUtils.convertDateTime(ZonedDateTime.now()))
                .type(PetsApiService.getPetTypeRecord(RandomUtils.getRandomNumber(1, 2)))
                .visits(List.of())
                .build();
        LOGGER.info(String.format("New instance of PetTDO is created: %s", petDTO.toString()));
        return petDTO;
    }

}
