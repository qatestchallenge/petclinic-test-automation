package org.petclinic.automation.data.dto;

import io.cucumber.core.internal.com.fasterxml.jackson.annotation.JsonProperty;

public record VisitResponseDTO(int id, String date, String description, @JsonProperty("pet") int petId) {
}
