package org.petclinic.automation.data.dto;

import lombok.Builder;

import java.util.List;

@Builder
public record OwnerDTO(Integer id, String firstName, String lastName, String address,
                       String city, String telephone, List<PetResponseDTO> pets) {
}
