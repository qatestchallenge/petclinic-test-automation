package org.petclinic.automation.data.dto;

import lombok.Builder;

@Builder
public record VisitDTO(int id, String date, String description, PetDTO pet) {
}
