package org.petclinic.automation.data.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;

@Builder
public record PetDTO(Integer id, String name, String birthDate, PetTypeDTO type,
                     @JsonProperty("ownerId") Integer owner, List<VisitResponseDTO> visits) {
}
