package org.petclinic.automation.data.dto;

import io.cucumber.core.internal.com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record PetResponseDTO(int id, String name, String birthDate,
                             PetTypeDTO type, @JsonProperty("owner") int ownerId, List<VisitResponseDTO> visits) {
}
