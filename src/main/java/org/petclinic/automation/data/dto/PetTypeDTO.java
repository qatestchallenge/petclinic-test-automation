package org.petclinic.automation.data.dto;

import lombok.Builder;

@Builder
public record PetTypeDTO(Integer id, String name) {
}
