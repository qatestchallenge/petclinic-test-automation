package org.petclinic.automation.data.dto;

import lombok.Builder;

@Builder
public record SpecialtyDTO(int id, String name) {
}
