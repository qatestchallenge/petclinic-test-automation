package org.petclinic.automation.data.dto;

import lombok.Builder;

import java.util.List;

@Builder
public record VetDTO(int id, String firstName, String lastName, List<SpecialtyDTO> specialties) {
}
