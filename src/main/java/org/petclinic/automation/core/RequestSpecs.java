package org.petclinic.automation.core;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.petclinic.automation.utils.UriUtils;

public class RequestSpecs {

    private RequestSpecs() {
    }

    public static RequestSpecification getBasicRequestSpecs() {
        return new RequestSpecBuilder()
                .setBaseUri(UriUtils.getBaseURI())
                .setRelaxedHTTPSValidation()
                .addFilter(new RestAssuredRequestFilter())
                .setAccept(ContentType.JSON.toString())
                .setContentType(ContentType.JSON)
                .build();
    }

}
