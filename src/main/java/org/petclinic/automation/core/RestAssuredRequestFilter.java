package org.petclinic.automation.core;

import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RestAssuredRequestFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(RestAssuredRequestFilter.class);

    @Override
    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext filterContext) {
        Response response = filterContext.next(requestSpec, responseSpec);
        LOGGER.info("-----------------------------------------------------------------------------------------");
        LOGGER.info(String.format("Request Method => %s\nRequest URI => %s\nRequest Headers => %s\nRequest Body => %s",
                requestSpec.getMethod(), requestSpec.getURI(), requestSpec.getHeaders(), requestSpec.getBody()));
        LOGGER.info(String.format("Response Status => %s\nResponse Body => %s", response.getStatusLine(), response.getBody().prettyPrint()));
        LOGGER.info("-----------------------------------------------------------------------------------------");
        return response;
    }
}
