package org.petclinic.automation.core;

import java.util.ResourceBundle;

public class ApiConfig {
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
    private static final String SCHEMA = "schema";
    private static final String HOST = "host";
    private static final String PORT = "port";
    private static final String BASE_PATH = "basePath";

    private ApiConfig() {
    }

    public static String getSchema() {
        return System.getProperty(SCHEMA, resourceBundle.getString(SCHEMA));
    }

    public static String getHost() {
        return System.getProperty(HOST, resourceBundle.getString(HOST));
    }

    public static int getPort() {
        return Integer.parseInt(System.getProperty(PORT, resourceBundle.getString(PORT)));
    }

    public static String getBasePath() {
        return System.getProperty(BASE_PATH, resourceBundle.getString(BASE_PATH));
    }

}
