package org.petclinic.automation.utils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {

    private DateTimeUtils() {
    }

    public static String convertDateTime(ZonedDateTime date) {
        return formatZonedDateTimeToString(date, "yyyy-MM-dd");
    }

    private static String formatZonedDateTimeToString(ZonedDateTime date, String dateFormat) {
        if (date == null) {
            return null;
        } else {
            return DateTimeFormatter.ofPattern(dateFormat).format(date);
        }
    }

}
