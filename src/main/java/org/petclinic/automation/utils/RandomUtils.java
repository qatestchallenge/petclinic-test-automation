package org.petclinic.automation.utils;

import java.util.Random;

public class RandomUtils {

    private RandomUtils() {
    }

    public static int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

}
