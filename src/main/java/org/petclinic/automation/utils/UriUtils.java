package org.petclinic.automation.utils;

import org.petclinic.automation.core.ApiConfig;
import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;

public class UriUtils {
    public static final Logger LOGGER = LogManager.getLogger(UriUtils.class);
    private static final String URL_ERROR = "Error in creating PetClinic base url";

    private UriUtils() {
    }

    public static URI getBaseURI() {
        URI uri = null;
        try {
            uri = new URIBuilder()
                    .setScheme(ApiConfig.getSchema())
                    .setHost(ApiConfig.getHost())
                    .setPort(ApiConfig.getPort())
                    .setPath(ApiConfig.getBasePath())
                    .build();
        } catch (URISyntaxException e) {
            LOGGER.error(URL_ERROR, e);
        }
        return uri;
    }

}
