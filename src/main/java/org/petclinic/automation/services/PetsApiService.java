package org.petclinic.automation.services;

import org.petclinic.automation.core.RequestSpecs;
import org.petclinic.automation.data.dto.PetDTO;
import org.petclinic.automation.data.dto.PetTypeDTO;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.given;

public class PetsApiService {
    private static final String PETS = "/pets";
    private static final String PET = PETS + "/%s";
    private static final String PET_TYPES = "/pettypes";
    private static final String PET_TYPE = "/pettypes/%s";

    private PetsApiService() {
    }

    public static Response getPets() {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .when()
                .get(PETS);
    }

    public static Response getPet(int petId) {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .when()
                .get(String.format(PET, petId));
    }

    public static Response postPet(PetDTO requestBody) {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .body(requestBody)
                .when()
                .post(PETS);
    }

    public static PetTypeDTO getPetTypeRecord(int petTypeId) {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .when()
                .get(String.format(PET_TYPE, petTypeId))
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response()
                .as(PetTypeDTO.class);
    }

    public static Response postPetType(PetTypeDTO requestBody) {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .body(requestBody)
                .when()
                .post(PET_TYPES);
    }

}
