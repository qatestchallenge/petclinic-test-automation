package org.petclinic.automation.services;

import org.petclinic.automation.core.RequestSpecs;
import org.petclinic.automation.data.dto.OwnerDTO;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class OwnersApiService {
    private static final String OWNERS = "/owners";
    private static final String OWNER = OWNERS + "/%s";

    private OwnersApiService() {
    }

    public static Response postOwner(OwnerDTO requestBody) {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .body(requestBody)
                .when()
                .post(OWNERS);
    }

    public static Response getOwners() {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .when()
                .get(OWNERS);
    }

    public static Response getOwner(int ownerId) {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .when()
                .get(String.format(OWNER, ownerId));
    }

    public static Response deleteOwner(int ownerId) {
        return given()
                .spec(RequestSpecs.getBasicRequestSpecs())
                .when()
                .delete(String.format(OWNER, ownerId));
    }

}
