package org.petclinic.automation.stepdefinitions;

import org.petclinic.automation.data.dto.PetDTO;
import org.petclinic.automation.data.factories.PetsFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.petclinic.automation.services.PetsApiService;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class PetsStepDefinitions  {
    private static final int INIT_PETS_COUNT = 13;
    private Response response;
    private PetDTO pet;
    private int petId;

    @When("user send a GET http request to pets endpoint")
    public void shouldGetAllPetsRecords() {
        response = PetsApiService.getPets();
    }

    @Then("user should receive valid HTTP response with {int} per pets schema")
    public void shouldValidateResponseStatusCode(Integer statusCode) {
        assertEquals(statusCode, response.getStatusCode(), "Response status code not matched");
    }

    @Then("user should match an array of pets")
    public void userShouldMatchAnArrayOfPets() {
        var pets = Arrays.asList(response.then().extract().response().as(PetDTO[].class));
        assertAll("pets",
                () -> assertFalse(pets.isEmpty(), "Pets records list aren't empty"),
                () -> assertTrue(pets.size() >= INIT_PETS_COUNT, "Pets records amount >= initial value")
        );
    }

    @Given("user has valid Pet data to init new record")
    public void shouldInitNewPetRecord() {
        pet = PetsFactory.getBasicPet();
    }

    @When("user send a POST http request to pets endpoint")
    public void shouldPostNewPetRecord() {
        response = PetsApiService.postPet(pet);
    }
    @Then("user should match details of pet")
    public void shouldMatchDetailsOfPet() {
        var petRecord = response.then().extract().as(PetDTO.class);
        assertAll("pet record",
                () -> assertTrue(petRecord.id() > INIT_PETS_COUNT, "Pet id records not added"),
                () -> assertEquals(pet.name(), petRecord.name(), "Pet name doesn't match"),
                () -> assertEquals(pet.birthDate(), petRecord.birthDate(), "Pet birthday doesn't match"),
                () -> assertEquals(pet.type().name(), petRecord.type().name(), "Pet type name doesn't match")
        );
    }

    @Given("user has existing Pet data record id")
    public void shouldInitExistingPetRecord() {
        pet = PetsFactory.getBasicPet();
        petId = PetsApiService.postPet(pet)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response()
                .as(PetDTO.class)
                .id();
    }
    @When("user send a GET http request to pets endpoint by id")
    public void shouldGetPetRecordById() {
        response = PetsApiService.getPet(petId);
    }

}
