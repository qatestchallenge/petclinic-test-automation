package org.petclinic.automation.stepdefinitions;

import org.petclinic.automation.data.dto.PetTypeDTO;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.petclinic.automation.services.PetsApiService;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTypesStepDefinitions {
    protected Response response;
    private PetTypeDTO petType;

    @Given("user has valid PetType {string} to init new record")
    public void shouldInitNewPetTypeRecord(String typeName) {
        petType = PetTypeDTO.builder().name(typeName).build();
    }
    @When("user send a POST http request to pet types endpoint")
    public void shouldPostNewPetTypeRecord() {
        response = PetsApiService.postPetType(petType);
    }

    @Then("user should receive valid HTTP response with {int} per pet types schema")
    public void shouldValidateResponseStatusCode(Integer statusCode) {
        assertEquals(statusCode, response.getStatusCode(), "Response status code not matched");
    }

    @Then("user should match details of pet type")
    public void userShouldMatchDetailsOfPetType() {
        var petTypeRecord = response.then().extract().response().as(PetTypeDTO.class);
        assertEquals(petType.name(), petTypeRecord.name(), "PetType name doesn't match");
    }

}
