package org.petclinic.automation.stepdefinitions;

import org.petclinic.automation.data.dto.OwnerDTO;
import org.petclinic.automation.data.factories.OwnersFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.petclinic.automation.services.OwnersApiService;

import java.util.Arrays;

import static org.apache.http.HttpStatus.SC_CREATED;
import static org.junit.jupiter.api.Assertions.*;

public class OwnersStepDefinitions {
    private static final int INIT_OWNERS_COUNT = 10;
    private Response response;
    private OwnerDTO owner;
    private int ownerId;

    @When("user send a GET http request to owners endpoint")
    public void shouldGetAllOwnersRecords() {
        response = OwnersApiService.getOwners();
    }

    @Then("user should receive valid HTTP response with {int} per owners schema")
    public void shouldValidateResponseStatusCode(Integer statusCode) {
        assertEquals(statusCode, response.getStatusCode(), "Response status code not matched");
    }

    @Then("user should match an array of pet owners")
    public void userShouldMatchAnArrayOfPetOwners() {
        var owners = Arrays.asList(response.then().extract().response().as(OwnerDTO[].class));
        assertAll("owners",
                () -> assertFalse(owners.isEmpty(), "Owners records list aren't empty"),
                () -> assertTrue(owners.size() >= INIT_OWNERS_COUNT, "Owners records amount >= initial value")
        );
    }

    @Given("user has valid Owner data to init new record")
    public void shouldInitNewOwnerRecord() {
        owner = OwnersFactory.getBasicOwner();
    }
    @When("user send a POST http request to owners endpoint")
    public void shouldPostNewOwnerRecord() {
        response = OwnersApiService.postOwner(owner);
    }
    @Then("user should match details of pet owner")
    public void shouldMatchDetailsOfPetOwner() {
        var ownerRecord = response.then().extract().as(OwnerDTO.class);
        assertAll("owner record",
                () -> assertTrue(ownerRecord.id() > INIT_OWNERS_COUNT, "Owner id records not added"),
                () -> assertEquals(owner.firstName(), ownerRecord.firstName(), "Owner first name doesn't match"),
                () -> assertEquals(owner.lastName(),  ownerRecord.lastName(), "Owner last name doesn't match"),
                () -> assertEquals(owner.address(), ownerRecord.address(), "Owner first name doesn't match"),
                () -> assertEquals(owner.city(), ownerRecord.city(), "Owner first city doesn't match"),
                () -> assertEquals(owner.telephone(), ownerRecord.telephone(), "Owner first telephone doesn't match"),
                () -> assertEquals(owner.pets().size(), ownerRecord.pets().size(), "Owner pets count doesn't match")
        );
    }

    @Given("user has existing Owner data record id")
    public void shouldInitExistingOwnerRecord() {
        owner = OwnersFactory.getBasicOwner();
        ownerId = OwnersApiService.postOwner(owner)
                .then()
                .statusCode(SC_CREATED)
                .extract().response().as(OwnerDTO.class)
                .id();
    }
    @When("user send a GET http request to owners endpoint by ownerId")
    public void shouldGetOwnerRecordById() {
        response = OwnersApiService.getOwner(ownerId);
    }

    @When("user send a DELETE http request to owners endpoint by ownerId")
    public void shouldDeleteOwnerRecordById() {
        response = OwnersApiService.deleteOwner(ownerId);
    }

    @Then("user should be deleted")
    public void shouldValidateOwnerWasRemoved() {
        response = OwnersApiService.getOwner(ownerId);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusCode(), "Owner record doesn't removed");
    }

}
