Feature: Test CRUD methods in endpoints related to pet owners

  @regression
  Scenario: Lists pet owners
    When user send a GET http request to owners endpoint
    Then user should receive valid HTTP response with 200 per owners schema
    And user should match an array of pet owners

  @regression
  @smoke
  Scenario: Adds a pet owner
    Given user has valid Owner data to init new record
    When user send a POST http request to owners endpoint
    Then user should receive valid HTTP response with 201 per owners schema
    And user should match details of pet owner

  @regression
  Scenario: Get a pet owner by Id
    Given user has existing Owner data record id
    When user send a GET http request to owners endpoint by ownerId
    Then user should receive valid HTTP response with 200 per owners schema
    And user should match details of pet owner

  @regression
  Scenario: Delete an owner by Id
    Given user has existing Owner data record id
    When user send a DELETE http request to owners endpoint by ownerId
    Then user should receive valid HTTP response with 204 per owners schema
    And user should be deleted