Feature: Test CRUD methods in endpoints related to pets

  @regression
  Scenario: Lists pets
    When user send a GET http request to pets endpoint
    Then user should receive valid HTTP response with 200 per pets schema
    And user should match an array of pets

  @regression
  @smoke
  Scenario: Create a pet
    Given user has valid Pet data to init new record
    When user send a POST http request to pets endpoint
    Then user should receive valid HTTP response with 200 per pets schema
    And user should match details of pet

  @regression
  Scenario: Get a pet by Id
    Given user has existing Pet data record id
    When user send a GET http request to pets endpoint by id
    Then user should receive valid HTTP response with 200 per pets schema
    And user should match details of pet