Feature: Test CRUD methods in endpoints related to pet types

  @regression
  @smoke
  Scenario Outline: Create a pet type
    Given user has valid PetType <name> to init new record
    When user send a POST http request to pet types endpoint
    Then user should receive valid HTTP response with 201 per pet types schema
    And user should match details of pet type
    Examples:
    | name |
    | "snake" |
    | "bird" |
    | "turtle" |
    | "fish" |
    | "hawk" |